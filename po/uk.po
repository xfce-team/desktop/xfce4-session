# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Xfce
# This file is distributed under the same license as the xfce4-session.master package.
# 
# Translators:
# Dmitry Nikitin <luckas_fb@mail.ru>, 2008
# dsafsadf <heneral@gmail.com>, 2016
# Gordon Freeman, 2023-2024
# Yarema aka Knedlyk <yupadmin@gmail.com>, 2016-2017,2019
# zubr139, 2016
# f977cb811fcf66493f48eac227fd7473_b98aa5d <2419f1d8d12d92441152e78ae3e3bde0_722180>, 2021
msgid ""
msgstr ""
"Project-Id-Version: Xfce4-session\n"
"Report-Msgid-Bugs-To: https://gitlab.xfce.org/\n"
"POT-Creation-Date: 2024-03-29 12:48+0100\n"
"PO-Revision-Date: 2013-07-02 20:44+0000\n"
"Last-Translator: Gordon Freeman, 2023-2024\n"
"Language-Team: Ukrainian (http://app.transifex.com/xfce/xfce4-session/language/uk/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: uk\n"
"Plural-Forms: nplurals=4; plural=(n % 1 == 0 && n % 10 == 1 && n % 100 != 11 ? 0 : n % 1 == 0 && n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 12 || n % 100 > 14) ? 1 : n % 1 == 0 && (n % 10 ==0 || (n % 10 >=5 && n % 10 <=9) || (n % 100 >=11 && n % 100 <=14 )) ? 2: 3);\n"

#: xfce.desktop.in:4
msgid "Xfce Session"
msgstr "Сеанс Xfce"

#: xfce.desktop.in:5 xfce-wayland.desktop.in:5
msgid "Use this session to run Xfce as your desktop environment"
msgstr "Використайте цей сеанс для запуску Xfce як Вашого типого середовища."

#: xfce-wayland.desktop.in:4
msgid "Xfce Session (Wayland)"
msgstr "Сеанс Xfce (Wayland)"

#: libxfsm/xfsm-util.c:370
msgid "Session"
msgstr "Сеанс"

#: libxfsm/xfsm-util.c:381
msgid "Last accessed"
msgstr "Останній доступ"

#: settings/main.c:100
msgid "Settings manager socket"
msgstr "Сокет менеджера властивостей"

#: settings/main.c:100
msgid "SOCKET ID"
msgstr "ID СОКЕТА"

#: settings/main.c:101
msgid "Version information"
msgstr "Інформація про версію"

#: settings/main.c:112 xfce4-session/main.c:354
#, c-format
msgid "Type '%s --help' for usage."
msgstr "Наберіть '%s --help' для використання."

#: settings/main.c:124 xfce4-session/main.c:364
#: xfce4-session-logout/main.c:132
msgid "The Xfce development team. All rights reserved."
msgstr "Група розробників Xfce. Усі права застережено."

#: settings/main.c:125 xfce4-session/main.c:365
#: xfce4-session-logout/main.c:135
#, c-format
msgid "Please report bugs to <%s>."
msgstr "Будь ласка повідомляйте про помилки для <%s>."

#: settings/main.c:134 xfce4-session/main.c:373
msgid "Unable to contact settings server"
msgstr "Неможливо зв'язатись з сервером властивостей"

#: settings/main.c:154
msgid "Unable to create user interface from embedded definition data"
msgstr "Неможливо створити інтерфейс користувача з типових вбудованих даних"

#: settings/main.c:168
msgid "App_lication Autostart"
msgstr "Авто_старт програми"

#: settings/main.c:174
msgid "Currently active session:"
msgstr "Поточний активний сеанс:"

#: settings/session-editor.c:63
msgid "If running"
msgstr "Якщо працює"

#: settings/session-editor.c:64
msgid "Always"
msgstr "Завжди"

#: settings/session-editor.c:65
msgid "Immediately"
msgstr "Відразу"

#: settings/session-editor.c:66
msgid "Never"
msgstr "Ніколи"

#: settings/session-editor.c:138
msgid "Session Save Error"
msgstr "Зберігати помилки сеансу"

#: settings/session-editor.c:139
msgid "Unable to save the session"
msgstr "Неможливо зберегти сеанс"

#: settings/session-editor.c:141 settings/session-editor.c:315
#: xfce4-session/xfsm-manager.c:1312 settings/xfce4-session-settings.ui:95
msgid "_Close"
msgstr "_Закрити"

#: settings/session-editor.c:200
msgid "Clear sessions"
msgstr "Очистити сесію"

#: settings/session-editor.c:201
msgid "Are you sure you want to empty the session cache?"
msgstr "Ви впевнені, що бажаєте очистити кеш сесії?"

#: settings/session-editor.c:202
msgid ""
"The saved states of your applications will not be restored during your next "
"login."
msgstr "Збережені стани Ваших програм не будуть відновлені у Вашому наступному сеансі."

#: settings/session-editor.c:203 settings/session-editor.c:290
#: settings/xfae-dialog.c:77 xfce4-session/xfsm-manager.c:712
#: xfce4-session/xfsm-logout-dialog.c:227
msgid "_Cancel"
msgstr "_Скасувати"

#: settings/session-editor.c:204
msgid "_Proceed"
msgstr "_Виконати"

#: settings/session-editor.c:242
#, c-format
msgid "You might need to delete some files manually in \"%s\"."
msgstr "Можливо Вам потрібно вилучити деякі файли вручну в \"%s\"."

#: settings/session-editor.c:245
msgid "All Xfce cache files could not be cleared"
msgstr "Всі файли кешу Xfce не можливо очистити"

#: settings/session-editor.c:284
#, c-format
msgid "Are you sure you want to terminate \"%s\"?"
msgstr "Ви впевнені, що бажаєте завершити \"%s\"?"

#: settings/session-editor.c:287 settings/session-editor.c:312
msgid "Terminate Program"
msgstr "Завершити програму"

#: settings/session-editor.c:289
msgid ""
"The application will lose any unsaved state and will not be restarted in "
"your next session."
msgstr "Програма втратить будь які незбережені зміни і не буде запускатись в наступному Вашому сеансі."

#: settings/session-editor.c:291 settings/xfce4-session-settings.ui:445
msgid "_Quit Program"
msgstr "Ви_йти з програми"

#: settings/session-editor.c:313
msgid "Unable to terminate program."
msgstr "Неможливо завершити програму."

#: settings/session-editor.c:541
msgid "(Unknown program)"
msgstr "(Невідома програма)"

#: settings/session-editor.c:790
msgid "Priority"
msgstr "Пріоритет"

#: settings/session-editor.c:800
msgid "PID"
msgstr "PID"

#: settings/session-editor.c:807 settings/xfae-window.c:198
msgid "Program"
msgstr "Програма"

#: settings/session-editor.c:833
msgid "Restart Style"
msgstr "Стиль перезавантаження"

#: settings/xfae-dialog.c:78 xfce4-session/xfsm-manager.c:714
msgid "_OK"
msgstr "_Гаразд"

#: settings/xfae-dialog.c:82 settings/xfae-window.c:252
msgid "Add application"
msgstr "Додати програму"

#: settings/xfae-dialog.c:96
msgid "Name:"
msgstr "Назва:"

#: settings/xfae-dialog.c:111
msgid "Description:"
msgstr "Опис:"

#: settings/xfae-dialog.c:125 settings/xfae-model.c:681
msgid "Command:"
msgstr "Команда:"

#: settings/xfae-dialog.c:138
msgid "Trigger:"
msgstr "Перемикач:"

#: settings/xfae-dialog.c:209
msgid "Select a command"
msgstr "Виберіть команду"

#: settings/xfae-dialog.c:212
msgid "Cancel"
msgstr "Скасувати"

#: settings/xfae-dialog.c:213
msgid "OK"
msgstr "Гаразд"

#: settings/xfae-dialog.c:260 settings/xfae-window.c:274
msgid "Edit application"
msgstr "Редагувати програму"

#: settings/xfae-model.c:114
msgid "on login"
msgstr "при вході"

#: settings/xfae-model.c:115
msgid "on logout"
msgstr "при виході"

#: settings/xfae-model.c:116
msgid "on shutdown"
msgstr "при вимиканні"

#: settings/xfae-model.c:117
msgid "on restart"
msgstr "при перезапуску"

#: settings/xfae-model.c:118
msgid "on suspend"
msgstr "при зупинці"

#: settings/xfae-model.c:119
msgid "on hibernate"
msgstr "при сплячці"

#: settings/xfae-model.c:120
msgid "on hybrid sleep"
msgstr "при гібридній сплячці"

#: settings/xfae-model.c:121
msgid "on switch user"
msgstr "при перемиканні користувача"

#: settings/xfae-model.c:463 settings/xfae-model.c:1177
#: settings/xfae-model.c:1235
#, c-format
msgid "Failed to open %s for writing"
msgstr "Не вдається відкрити файл %s для запису"

#: settings/xfae-model.c:815
#, c-format
msgid "Failed to unlink %s: %s"
msgstr "Не вдається видалити %s: %s"

#: settings/xfae-model.c:977
#, c-format
msgid "Failed to create file %s"
msgstr "Не вдається створити файл %s"

#: settings/xfae-model.c:1001
#, c-format
msgid "Failed to write file %s"
msgstr "Не вдається записати файл %s"

#: settings/xfae-model.c:1061
#, c-format
msgid "Failed to open %s for reading"
msgstr "Не вдається відкрити %s для читання"

#: settings/xfae-window.c:108
msgid "Failed to set run hook"
msgstr "Помилка запуску скрипта"

#: settings/xfae-window.c:223
msgid "Trigger"
msgstr "Перемикач:"

#: settings/xfae-window.c:249 settings/xfae-window.c:338
msgid "Add"
msgstr "Додати"

#: settings/xfae-window.c:258 settings/xfae-window.c:344
msgid "Remove"
msgstr "Вилучити"

#: settings/xfae-window.c:261
msgid "Remove application"
msgstr "Вилучити програму"

#: settings/xfae-window.c:271
msgid "Edit"
msgstr "Редагувати"

#: settings/xfae-window.c:397
#, c-format
msgid "Failed adding \"%s\""
msgstr "Не вдалось додати \"%s\""

#: settings/xfae-window.c:428 settings/xfae-window.c:442
msgid "Failed to remove item"
msgstr "Не вдається видалити елемент"

#: settings/xfae-window.c:471
msgid "Failed to edit item"
msgstr "Не вдається відредагувати елемент"

#: settings/xfae-window.c:491
#, c-format
msgid "Failed to edit item \"%s\""
msgstr "Не вдається відредагувати елемент \"%s\""

#: settings/xfae-window.c:519
msgid "Failed to toggle item"
msgstr "Не вдається перемкнути елемент"

#: xfce4-session/main.c:79
msgid "Disable binding to TCP ports"
msgstr "Заборонити з'єднання на порти TCP"

#: xfce4-session/main.c:80 xfce4-session-logout/main.c:90
msgid "Print version information and exit"
msgstr "Надрукувати інформацію про версію і вийти"

#: xfce4-session/xfsm-chooser.c:147
msgid "Session Manager"
msgstr "Менеджер сеансів"

#: xfce4-session/xfsm-chooser.c:168
msgid ""
"Choose the session you want to restore. You can simply double-click the "
"session name to restore it."
msgstr "Виберіть сеанс, який бажаєте відновити. Щоб завантажити сеанс, можете просто двічі клацнути на його назві."

#: xfce4-session/xfsm-chooser.c:184
msgid "Create a new session."
msgstr "Створити новий сеанс."

#: xfce4-session/xfsm-chooser.c:191
msgid "Delete a saved session."
msgstr "Вилучити записаний сеанс."

#. "Logout" button
#: xfce4-session/xfsm-chooser.c:202
#: xfce4-session-logout/xfce4-session-logout.desktop.in:11
msgid "Log Out"
msgstr "Вийти"

#: xfce4-session/xfsm-chooser.c:204
msgid "Cancel the login attempt and return to the login screen."
msgstr "Скасувати спробу входу та повернутись до екрану входу у систему."

#. "Start" button
#: xfce4-session/xfsm-chooser.c:211
msgid "Start"
msgstr "Запуск"

#: xfce4-session/xfsm-chooser.c:212
msgid "Start an existing session."
msgstr "Запустити існуючий сеанс."

#: xfce4-session/xfsm-dns.c:78
msgid "(Unknown)"
msgstr "(невідомо)"

#: xfce4-session/xfsm-dns.c:152
#, c-format
msgid ""
"Could not look up internet address for %s.\n"
"This will prevent Xfce from operating correctly.\n"
"It may be possible to correct the problem by adding\n"
"%s to the file /etc/hosts on your system."
msgstr "Не вдається визначити IP-адресу вузла %s.\nЧерез це Xfce може працювати некоректно.\nПроблему можна вирішити, якщо додати вузол %s\nу файл /etc/hosts вашої системи."

#: xfce4-session/xfsm-dns.c:159
msgid "Continue anyway"
msgstr "Продовжити"

#: xfce4-session/xfsm-dns.c:160
msgid "Try again"
msgstr "Повторити спробу"

#: xfce4-session/xfsm-manager.c:581
#, c-format
msgid ""
"Unable to determine failsafe session name.  Possible causes: xfconfd isn't "
"running (D-Bus setup problem); environment variable $XDG_CONFIG_DIRS is set "
"incorrectly (must include \"%s\"), or xfce4-session is installed "
"incorrectly."
msgstr "Неможливо визначити назву безпечного сеансу. Можливі причини: xconfd не запущено (проблема налаштування D-Bus); неправильно встановлено змінну середовища $XDG_CONFIG_DIRS (має включати \"%s\"), або неправильно встановлено xfce4-session."

#: xfce4-session/xfsm-manager.c:606
#, c-format
msgid ""
"The specified failsafe session (\"%s\") is not marked as a failsafe session."
msgstr "Вибраний безпечний сеанс (\"%s\") не позначено як безпечний сеанс."

#: xfce4-session/xfsm-manager.c:640
msgid "The list of applications in the failsafe session is empty."
msgstr "Список програм в безпечному сеансі пустий."

#: xfce4-session/xfsm-manager.c:726
msgid "Name for the new session"
msgstr "Назва нового сеансу"

#. FIXME: migrate this into the splash screen somehow so the
#. * window doesn't look ugly (right now no WM is running, so it
#. * won't have window decorations).
#: xfce4-session/xfsm-manager.c:804
msgid "Session Manager Error"
msgstr "Помилка менеджеру сеансів"

#: xfce4-session/xfsm-manager.c:806
msgid "Unable to load a failsafe session"
msgstr "Неможливо завантажити безпечний сеанс"

#: xfce4-session/xfsm-manager.c:808
msgid "_Quit"
msgstr "Ви_йти"

#: xfce4-session/xfsm-manager.c:1302
msgid "Shutdown Failed"
msgstr "Неможливо вимкнути"

#: xfce4-session/xfsm-manager.c:1305
msgid "Failed to suspend session"
msgstr "Невдача призупинення сеансу"

#: xfce4-session/xfsm-manager.c:1307
msgid "Failed to hibernate session"
msgstr "Невдача для сплячого сеансу"

#: xfce4-session/xfsm-manager.c:1309
msgid "Failed to hybrid sleep session"
msgstr "Невдача гібридної сплячки сеансу"

#: xfce4-session/xfsm-manager.c:1310
msgid "Failed to switch user"
msgstr "Неможливо перемкнути користувача"

#: xfce4-session/xfsm-manager.c:1637
msgid "Can only terminate clients when in the idle state"
msgstr "Можна тільки завершити клієнтів коли перебуваєш в стані очікування"

#: xfce4-session/xfsm-manager.c:2320
msgid "Session manager must be in idle state when requesting a checkpoint"
msgstr "Менеджер сеансу мусить бути в стані очікування коли запитується місце перевірки"

#: xfce4-session/xfsm-manager.c:2390 xfce4-session/xfsm-manager.c:2410
msgid "Session manager must be in idle state when requesting a shutdown"
msgstr "Менеджер сеансу мусить бути в стані очікування коли запитується вимкнення"

#: xfce4-session/xfsm-manager.c:2447
msgid "Session manager must be in idle state when requesting a restart"
msgstr "Менеджер сеансу мусить бути в стані очікування, коли дається запит на перезавантаження"

#: xfce4-session/xfsm-manager.c:2483 xfce4-session/xfsm-manager.c:2522
#: xfce4-session/xfsm-manager.c:2560
msgid "Session manager must be in idle state when requesting a sleep"
msgstr "Менеджер сеансів мусить бути в стані очікування, коли надсилається запит на сплячий режим"

#: xfce4-session/xfsm-manager.c:2687
msgid "Session manager must be in idle state when requesting a user switch"
msgstr "Менеджер сеансів мусить бути в стані очікування, коли надсилається запит на перемикання користувача"

#: xfce4-session/xfsm-logout-dialog.c:199
#, c-format
msgid "Log out %s"
msgstr "Вийти %s"

#: xfce4-session/xfsm-logout-dialog.c:245
msgid "_Log Out"
msgstr "_Вийти"

#: xfce4-session/xfsm-logout-dialog.c:264
msgid "_Restart and update"
msgstr "_Перезавантажити та оновити"

#: xfce4-session/xfsm-logout-dialog.c:264
msgid "_Restart"
msgstr "_Перезавантажити"

#: xfce4-session/xfsm-logout-dialog.c:278
msgid "Update and Shut _Down"
msgstr "Оновити та ви_мкнути"

#: xfce4-session/xfsm-logout-dialog.c:278
msgid "Shut _Down"
msgstr "Ви_мкнути"

#: xfce4-session/xfsm-logout-dialog.c:302
msgid "Sus_pend"
msgstr "_Призупинити"

#: xfce4-session/xfsm-logout-dialog.c:322
msgid "_Hibernate"
msgstr "_В сплячку"

#: xfce4-session/xfsm-logout-dialog.c:342
msgid "H_ybrid Sleep"
msgstr "_Гібридна сплячка"

#: xfce4-session/xfsm-logout-dialog.c:366
msgid "Switch _User"
msgstr "Переключити _користувача"

#: xfce4-session/xfsm-logout-dialog.c:406
msgid "An error occurred"
msgstr "Сталася помилка"

#: xfce4-session/xfsm-shutdown.c:171
msgid "Shutdown is blocked by the kiosk settings"
msgstr "Завершення заблоковано налаштуванням kiosk"

#: xfce4-session/xfsm-shutdown.c:228
#, c-format
msgid "Unknown shutdown method %d"
msgstr "Невідомий метод завершення %d"

#: xfce4-session-logout/main.c:58
msgid "Log out without displaying the logout dialog"
msgstr "Завершити сеанс без діалогу підтвердження виходу"

#: xfce4-session-logout/main.c:62
msgid "Halt without displaying the logout dialog"
msgstr "Вимкнути комп'ютер без діалогу підтвердження виходу"

#: xfce4-session-logout/main.c:66
msgid "Reboot without displaying the logout dialog"
msgstr "Перезавантажити без діалогу підтвердження виходу"

#: xfce4-session-logout/main.c:70
msgid "Suspend without displaying the logout dialog"
msgstr "Призупинити комп'ютер без діалогу підтвердження виходу"

#: xfce4-session-logout/main.c:74
msgid "Hibernate without displaying the logout dialog"
msgstr "Приспати без діалогу підтвердження виходу"

#: xfce4-session-logout/main.c:78
msgid "Hybrid Sleep without displaying the logout dialog"
msgstr "Гібридна сплячка без діалогу підтвердження виходу"

#: xfce4-session-logout/main.c:82
msgid "Switch user without displaying the logout dialog"
msgstr "Перемикання користувача без відображення діалогового вікна входу в систему"

#: xfce4-session-logout/main.c:86
msgid "Log out quickly; don't save the session"
msgstr "Вийти якнайшвидше; дані сеансу не зберігати"

#: xfce4-session-logout/main.c:108
msgid "Unknown error"
msgstr "Невідома помилка"

#: xfce4-session-logout/main.c:133
msgid "Written by Benedikt Meurer <benny@xfce.org>"
msgstr "Написано Benedikt Meurer <benny@xfce.org>."

#: xfce4-session-logout/main.c:134
msgid "and Brian Tarricone <kelnos@xfce.org>."
msgstr "і Brian Tarricone <kelnos@xfce.org>."

#: xfce4-session-logout/main.c:154 xfce4-session-logout/main.c:229
msgid "Received error while trying to log out"
msgstr "Отримано помилку під час спроби вийти з сеансу"

#: xfce4-session-logout/xfce4-session-logout.desktop.in:12
msgid "Log out of the Xfce Desktop"
msgstr "Вийти з середовища Xfce"

#. SECURITY:
#. - A normal active user on the local machine does not need permission
#. to suspend or hibernate their system.
#: xfce4-session/org.xfce.session.policy.in.in:23
msgid "Shutdown, restart, suspend, or hibernate the system"
msgstr "Вимкнення, перезапуск, призупинення або сплячий режим системи"

#: xfce4-session/org.xfce.session.policy.in.in:24
msgid ""
"Authentication is required to shutdown, restart, suspend, or hibernate the "
"system."
msgstr "Автентифікація потрібна для вимкнення, перезапуску, призупинення або переходу системи в сплячий режим."

#: settings/xfce-session-settings.desktop.in:3
#: settings/xfce4-session-settings.ui:61
msgid "Session and Startup"
msgstr "Сеанси та запуск"

#: settings/xfce-session-settings.desktop.in:4
msgid "Customize desktop startup"
msgstr "Налаштувати запуск стільниці"

#: settings/xfce-session-settings.desktop.in:11
msgid ""
"session;settings;preferences;manager;startup;login;logout;shutdown;lock "
"screen;application;autostart;launch;services;daemon;agent;"
msgstr "сеанс;налаштування;властивості;керівник;завантаження;вхід;вихід;завершення;замикання екрану;програма;автостарт;запуск;сервіси;демон;аґент;"

#: settings/xfce4-session-settings.ui:51
msgid ""
"These applications are a part of the currently-running session,\n"
"and can be saved now or when you log out.\n"
"Changes below will only take effect when the session is saved."
msgstr "Ці програми є частиною запущеного зараз сеансу, і можуть бути\nзбережені зараз або коли Ви вийдете з сеансу. Зміни внесені\nнижче матимуть ефект лише після збереження сеансу."

#: settings/xfce4-session-settings.ui:79
msgid "_Help"
msgstr "_Довідка"

#: settings/xfce4-session-settings.ui:144
msgid "_Display chooser on login"
msgstr "_Відображати програму вибору сеансу при вході"

#: settings/xfce4-session-settings.ui:149
msgid "Display the session chooser every time Xfce starts"
msgstr "Відображати програму вибору сеансу кожного разу при старті Xfce"

#: settings/xfce4-session-settings.ui:161
msgid "<b>Session Chooser</b>"
msgstr "<b>Програма вибору сеансу</b>"

#: settings/xfce4-session-settings.ui:192
msgid "Automatically save session on logo_ut"
msgstr "Автоматично зберігати сеанс при вихо_ді"

#: settings/xfce4-session-settings.ui:197
msgid "Always save the session when logging out"
msgstr "Завжди зберігати сеанс при виході"

#: settings/xfce4-session-settings.ui:210
msgid "Pro_mpt on logout"
msgstr "За_питувати при виході"

#: settings/xfce4-session-settings.ui:215
msgid "Prompt for confirmation when logging out"
msgstr "Запитувати підтвердження при виході з сеансу"

#: settings/xfce4-session-settings.ui:235
msgid "<b>Logout Settings</b>"
msgstr "<b>Параметри виходу</b>"

#: settings/xfce4-session-settings.ui:266
msgid "Lock screen be_fore sleep"
msgstr "Заблокувати екран пе_ред сном"

#: settings/xfce4-session-settings.ui:289
msgid "<b>Shutdown</b>"
msgstr "<b>Завершити</b>"

#: settings/xfce4-session-settings.ui:306
msgid "_General"
msgstr "_Загальні"

#: settings/xfce4-session-settings.ui:330
msgid "Save Sess_ion"
msgstr "Зберегти се_анс"

#: settings/xfce4-session-settings.ui:384
msgid "Currently active session: <b>Default</b>"
msgstr "Поточний активний сеанс: <b>Default</b>"

#: settings/xfce4-session-settings.ui:485
msgid "Current Sessio_n"
msgstr "Поточний сеан_с"

#: settings/xfce4-session-settings.ui:536
msgid "_Remove"
msgstr "В_илучити"

#: settings/xfce4-session-settings.ui:540
msgid "Delete the selected session"
msgstr "Вилучити вибраний сеанс"

#: settings/xfce4-session-settings.ui:564
msgid "Clear Save_d Sessions"
msgstr "Очистити збереже_ні сеанси"

#: settings/xfce4-session-settings.ui:614
msgid "Saved _Sessions"
msgstr "Збережені се_анси"

#: settings/xfce4-session-settings.ui:649
msgid "Launch GN_OME services on startup"
msgstr "Запускати служби ГН_ОМА при завантажені"

#: settings/xfce4-session-settings.ui:654
msgid "Start GNOME services, such as gnome-keyring"
msgstr "Запускати служби ГНОМА, такі як gnome-keyring та GNOME accessibility framework"

#: settings/xfce4-session-settings.ui:667
msgid "Launch _KDE services on startup"
msgstr "Запускати служби _KDE при завантажені"

#: settings/xfce4-session-settings.ui:672
msgid "Start KDE services, such as kdeinit"
msgstr "Запускати служби KDE, такі як kdeinit"

#: settings/xfce4-session-settings.ui:691
msgid "<b>Compatibility</b>"
msgstr "<b>Сумісність</b>"

#: settings/xfce4-session-settings.ui:716
msgid "Manage _remote applications"
msgstr "_Керувати віддаленими програмами"

#: settings/xfce4-session-settings.ui:721
msgid ""
"Manage remote applications over the network (this may be a security risk)"
msgstr "Керувати віддаленими програмами по мережі (це пов'язано з ризиком безпеки)"

#: settings/xfce4-session-settings.ui:733
msgid "<b>Security</b>"
msgstr "<b>Безпека</b>"

#: settings/xfce4-session-settings.ui:753
msgid "Ad_vanced"
msgstr "_Додаткові"

#: settings/xfce4-session-settings.ui:784
msgid "Saving Session"
msgstr "Збереження сеансу"

#: settings/xfce4-session-settings.ui:860
msgid ""
"Your session is being saved.  If you do not wish to wait, you may close this"
" window."
msgstr "Ваш сеанс було збережено. Якщо Ви не бажаєте чекати, то можете закрити це вікно."
